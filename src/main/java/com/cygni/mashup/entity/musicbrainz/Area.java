package com.cygni.mashup.entity.musicbrainz;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Area {
    @JsonProperty("iso-3166-1-codes")
    private String[] iso31661codes;
    private String disambiguation;
    private String name;
    private String id;
    @JsonProperty("sort-name")
    private String sortName;

    public String[] getIso31661codes() {
        return iso31661codes;
    }

    public void setIso31661codes(String[] iso31661codes) {
        this.iso31661codes = iso31661codes;
    }

    public String getDisambiguation() {
        return disambiguation;
    }

    public void setDisambiguation(String disambiguation) {
        this.disambiguation = disambiguation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSortName() {
        return sortName;
    }

    public void setSortName(String sortName) {
        this.sortName = sortName;
    }
}
