package com.cygni.mashup.entity.musicbrainz;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

public class Relation {
    @JsonProperty("source-credit")
    private String sourceCredit;
    @JsonProperty("attribute-ids")
    private Map<String,Object> attributeIds;
    @JsonProperty("type-id")
    private String typeId;
    @JsonProperty("attribute-values")
    private Map<String,Object> attributeValues;
    @JsonProperty("target-type")
    private String targetType;
    private String type;
    @JsonProperty("target-credit")
    private String targetCredit;
    private Url url;
    private String ended;
    private String end;
    private String[] attributes;
    private String begin;
    private String direction;

    public String getSourceCredit() {
        return sourceCredit;
    }

    public void setSourceCredit(String sourceCredit) {
        this.sourceCredit = sourceCredit;
    }

    public Map<String, Object> getAttributeIds() {
        return attributeIds;
    }

    public void setAttributeIds(Map<String, Object> attributeIds) {
        this.attributeIds = attributeIds;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public Map<String,Object> getAttributeValues() {
        return attributeValues;
    }

    public void setAttributeValues(Map<String,Object> attributeValues) {
        this.attributeValues = attributeValues;
    }

    public String getTargetType() {
        return targetType;
    }

    public void setTargetType(String targetType) {
        this.targetType = targetType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTargetCredit() {
        return targetCredit;
    }

    public void setTargetCredit(String targetCredit) {
        this.targetCredit = targetCredit;
    }

    public Url getUrl() {
        return url;
    }

    public void setUrl(Url url) {
        this.url = url;
    }

    public String getEnded() {
        return ended;
    }

    public void setEnded(String ended) {
        this.ended = ended;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String[] getAttributes() {
        return attributes;
    }

    public void setAttributes(String[] attributes) {
        this.attributes = attributes;
    }

    public String getBegin() {
        return begin;
    }

    public void setBegin(String begin) {
        this.begin = begin;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }
}
