package com.cygni.mashup.entity.musicbrainz;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Artist {
    private Area area;
    private String[] ipis;
    @JsonProperty("end-area")
    private String endArea;
    @JsonProperty("begin-area")
    private BeginArea beginArea;
    private String country;
    private String gender;
    @JsonProperty("life-span")
    private LifeSpan lifeSpan;
    @JsonProperty("type-id")
    private String typeId;
    private String type;
    @JsonProperty("gender-id")
    private String genderId;
    private String[] isnis;
    private String disambiguation;
    private String name;
    @JsonProperty("release-groups")
    private List<ReleaseGroup> releaseGroups;
    private String id;
    @JsonProperty("sort-name")
    private String sortName;
    private List<Relation> relations;

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public String[] getIpis() {
        return ipis;
    }

    public void setIpis(String[] ipis) {
        this.ipis = ipis;
    }

    public String getEndArea() {
        return endArea;
    }

    public void setEndArea(String endArea) {
        this.endArea = endArea;
    }

    public BeginArea getBeginArea() {
        return beginArea;
    }

    public void setBeginArea(BeginArea beginArea) {
        this.beginArea = beginArea;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public LifeSpan getLifeSpan() {
        return lifeSpan;
    }

    public void setLifeSpan(LifeSpan lifeSpan) {
        this.lifeSpan = lifeSpan;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGenderId() {
        return genderId;
    }

    public void setGenderId(String genderId) {
        this.genderId = genderId;
    }

    public String[] getIsnis() {
        return isnis;
    }

    public void setIsnis(String[] isnis) {
        this.isnis = isnis;
    }

    public String getDisambiguation() {
        return disambiguation;
    }

    public void setDisambiguation(String disambiguation) {
        this.disambiguation = disambiguation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ReleaseGroup> getReleaseGroups() {
        return releaseGroups;
    }

    public void setReleaseGroups(List<ReleaseGroup> releaseGroups) {
        this.releaseGroups = releaseGroups;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSortName() {
        return sortName;
    }

    public void setSortName(String sortName) {
        this.sortName = sortName;
    }

    public List<Relation> getRelations() {
        return relations;
    }

    public void setRelations(List<Relation> relations) {
        this.relations = relations;
    }
}
