package com.cygni.mashup.entity.musicbrainz;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ReleaseGroup {
    @JsonProperty("primary-type-id")
    private String primaryTypeId;
    @JsonProperty("primary-type")
    private String primaryType;
    @JsonProperty("secondary-types")
    private String[] secondaryTypes;
    private String disambiguation;
    @JsonProperty("secondary-type-ids")
    private String[] secondaryTypeIds;
    private String id;
    private String title;
    @JsonProperty("first-release-date")
    private String firstReleaseDate;

    public String getPrimaryTypeId() {
        return primaryTypeId;
    }

    public void setPrimaryTypeId(String primaryTypeId) {
        this.primaryTypeId = primaryTypeId;
    }

    public String getPrimaryType() {
        return primaryType;
    }

    public void setPrimaryType(String primaryType) {
        this.primaryType = primaryType;
    }

    public String[] getSecondaryTypes() {
        return secondaryTypes;
    }

    public void setSecondaryTypes(String[] secondaryTypes) {
        this.secondaryTypes = secondaryTypes;
    }

    public String getDisambiguation() {
        return disambiguation;
    }

    public void setDisambiguation(String disambiguation) {
        this.disambiguation = disambiguation;
    }

    public String[] getSecondaryTypeIds() {
        return secondaryTypeIds;
    }

    public void setSecondaryTypeIds(String[] secondaryTypeIds) {
        this.secondaryTypeIds = secondaryTypeIds;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstReleaseDate() {
        return firstReleaseDate;
    }

    public void setFirstReleaseDate(String firstReleaseDate) {
        this.firstReleaseDate = firstReleaseDate;
    }
}
