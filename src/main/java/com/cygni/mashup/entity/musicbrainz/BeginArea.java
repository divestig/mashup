package com.cygni.mashup.entity.musicbrainz;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BeginArea {
    private String name;
    private String disambiguation;
    @JsonProperty("sort-name")
    private String sortName;
    private String id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisambiguation() {
        return disambiguation;
    }

    public void setDisambiguation(String disambiguation) {
        this.disambiguation = disambiguation;
    }

    public String getSortName() {
        return sortName;
    }

    public void setSortName(String sortName) {
        this.sortName = sortName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
