package com.cygni.mashup.entity.wikidata;

public class SiteLink {
    private String site;
    private String title;

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
