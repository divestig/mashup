package com.cygni.mashup.entity.wikidata;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Wikidata {
    private Object entities;

    public Object getEntities() {
        return entities;
    }

    public void setEntities(Object entities) {
        this.entities = entities;
    }
}
