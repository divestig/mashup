package com.cygni.mashup.models;

import org.springframework.http.HttpStatus;

public class WrappedApiResponse<T> {
    private T Result;
    private HttpStatus statusCode;
    private Exception exception;

    public T getResult() {
        return Result;
    }

    public void setResult(T result) {
        Result = result;
    }

    public HttpStatus getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(HttpStatus statusCode) {
        this.statusCode = statusCode;
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }
}
