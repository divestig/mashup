package com.cygni.mashup;

import com.cygni.mashup.entity.coverart.CoverImage;
import com.cygni.mashup.entity.musicbrainz.Artist;
import com.cygni.mashup.models.WrappedApiResponse;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URLEncoder;
import java.util.concurrent.Callable;


@Component
public class DataFetcher {
    public WrappedApiResponse<Artist> getArtist(String mbid) {
        return getData(()-> {
            ResponseEntity<Artist> response = getResponse("http://musicbrainz.org/ws/2/artist/"+mbid+"?&fmt=json&inc=url-rels+release-groups", Artist.class);
            return response.getBody();
        });
    }

    public WrappedApiResponse<String> getWikiData(String wikiDataId) {
        return getData(()-> {
            ResponseEntity<String> response = getResponse("https://www.wikidata.org/w/api.php?action=wbgetentities&ids=" + wikiDataId + "&format=json&props=sitelinks", String.class);
            ObjectMapper om = new ObjectMapper();
            JsonNode root = om.readTree(response.getBody());
            return root.path("entities").path(wikiDataId).path("sitelinks").path("enwiki").get("title").asText();
        });
    }

    public WrappedApiResponse<String> getWikiDescription(String query) {
        return getData(()-> {
            //URLencode breaks link, space works.
            ResponseEntity<String> response = getResponse("https://en.wikipedia.org/w/api.php?action=query&format=json&prop=extracts&exintro=true&redirects=true&titles=" + query, String.class);
            ObjectMapper om = new ObjectMapper();
            JsonNode root = om.readTree(response.getBody());

            return root.path("query").path("pages").iterator().next().get("extract").asText();
        });
    }

    public WrappedApiResponse<CoverImage> getCoverArt(String mbid) {
        return getData(()-> {
            ResponseEntity<String> response = getResponse("http://coverartarchive.org/release-group/" + mbid, String.class);
            ObjectMapper om = new ObjectMapper();
            om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            URLEncoder.encode(mbid);
            JsonNode root = om.readTree(response.getBody());
            return om.convertValue(root.path("images").iterator().next(), CoverImage.class);
        });
    }

    private ResponseEntity getResponse(String url, Class c) {
        RestTemplate restTemplate = new RestTemplate();
            return restTemplate.getForEntity(url, c);
    }

    private <T> WrappedApiResponse<T> getData(Callable<T> callable ) {
        WrappedApiResponse<T> response = new WrappedApiResponse<>();
        try {
            response.setResult(callable.call());
        }catch(HttpClientErrorException | HttpServerErrorException e) {
            response.setStatusCode(e.getStatusCode());
            response.setException(e);
        }catch(Exception e) {
            response.setException(e);
        }
        return response;
    }
}
