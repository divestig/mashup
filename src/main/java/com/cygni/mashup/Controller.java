package com.cygni.mashup;

import com.cygni.mashup.models.Mashup;
import com.weddini.throttling.Throttling;
import com.weddini.throttling.ThrottlingType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.TimeUnit;

@RestController
@CacheConfig(cacheNames = {"mashups"})
public class Controller {

    @Autowired
    DataFetcher dataFetcher;

    @Autowired
    DataCombiner dataCombiner;

    @RequestMapping(
            value = "/mashup/{mbid}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    @Cacheable
    @Throttling(type = ThrottlingType.RemoteAddr, limit = 1, timeUnit = TimeUnit.SECONDS)
    public ResponseEntity<Mashup> getMashup(
            @PathVariable(value = "mbid") String mbid
    ) throws Exception {
        Mashup response = dataCombiner.createMashup(mbid);
        if (response != null) {
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }
}
