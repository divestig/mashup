package com.cygni.mashup;

import com.cygni.mashup.entity.coverart.CoverImage;
import com.cygni.mashup.entity.musicbrainz.Artist;
import com.cygni.mashup.entity.musicbrainz.Relation;
import com.cygni.mashup.entity.musicbrainz.ReleaseGroup;
import com.cygni.mashup.models.Album;
import com.cygni.mashup.models.Mashup;
import com.cygni.mashup.models.WrappedApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class DataCombiner {

    @Autowired
    DataFetcher dataFetcher;


    public Mashup createMashup(String mbid) throws Exception {
        WrappedApiResponse<Artist> response = dataFetcher.getArtist(mbid);
        if (response.getException() == null) {
            Mashup mashup = new Mashup();
            Artist artist = response.getResult();
            String wikidataId = findWikidataId(artist.getRelations());
            List<Album> albums = artist.getReleaseGroups().parallelStream()
                    .map(this::createAlbumWithCover).collect(Collectors.toList());

            WrappedApiResponse<String> englishWikiQuery = dataFetcher.getWikiData(wikidataId);
            if (englishWikiQuery.getException() == null) {
                String wikiQuery = englishWikiQuery.getResult();
                mashup.setDescription(
                        dataFetcher.getWikiDescription(wikiQuery).getResult()
                );
            }

            mashup.setMbid(artist.getId());
            mashup.setAlbums(albums);
            return mashup;
        }
        throw new Exception("Error creating result");
    }
    private Album createAlbumWithCover(ReleaseGroup release) {
        Album album = new Album();
        album.setTitle(release.getTitle());
        album.setId(release.getId());
        album.setReleaseDate(release.getFirstReleaseDate());
        WrappedApiResponse<CoverImage> response = dataFetcher.getCoverArt(release.getId());
        if (response.getException() == null) {
            album.setImageUrl(response.getResult().getImage());
        }
        return album;
    }

    private String findWikidataId (List<Relation> relations) {
        return relations.stream()
                .filter(relation -> "wikidata".equals(relation.getType()))
                .findAny()
                .orElse(null)
                .getUrl()
                .getResource()
                .replace("https://www.wikidata.org/wiki/", "");
    }
}
