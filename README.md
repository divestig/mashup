Installation
    Installera Java - https://java.com/sv/download/
    Kontrollera så att porten 8080 ej är upptagen
    Starta filen “mashup-0.1.jar”

    Gör förfrågan mot localhost:8080/ eller port :8080 på er serveradress
    API: mashup/id
    Exempelfråga: http://localhost:8080/mashup/5b11f4ce-a62d-471e-81fc-a69a8278c7da
